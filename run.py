# from flask import Flask
from flask_cors import CORS
import flask

def create_app(config_filename):
    app = flask.Flask(__name__)
    CORS(app)
    app.config.from_object(config_filename)


    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from Model import db
    # with app.app_context():
    db.init_app(app)

    return app


if __name__ == "__main__":
    app = create_app("config")
    app.run(debug=True, port=3090)
