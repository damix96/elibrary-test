from flask import Blueprint
from flask_restful import Api
from resources.Auth import Auth
from resources.Languages import Languages
from resources.Users import Users, checkUser, changePassword
from resources.Regions import Regions
from resources.Areas import Areas
from resources.Schools import Schools, GetStudentCountSchool
from resources.Grades import Grades, DeleteGrade
from resources.Subject import Subjects
from resources.Formbooks import Formbooks
from resources.Students import Students, DeleteStudent
from resources.Students_books import Student_books, DeleteStudent_book, GetBook
from resources.Publishers import Publishers
from resources.Books import Books
from resources.StudyYear import StudyYears
from resources.Seed import Seed
from resources.Orders import Orders, ShipOrder, SortByPublisher
from resources.Requests import Requests, DirectorRequest, AreaRequest, RequestsByOrders, ResendByLibrarian, FinishRequest
from resources.Requestedbooks import sortedRequestedBooks, Requestedbooks, DeleteRequestedBook
from resources.Grades import Agregated
from resources.Requests import AgregatedRequest
from resources.Katos import Katos
from resources.Formbooks import GetSubjectsPublisherLanguages, DeleteFormbook
from resources.getLogs import Logs, PaginateLog


api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Routes

api.add_resource(Auth, '/auth')
api.add_resource(Users, '/user')
api.add_resource(checkUser, '/checkLogin')
api.add_resource(changePassword, '/changePassword')
api.add_resource(Students, '/student')
api.add_resource(Student_books, '/studentBooks')
api.add_resource(GetBook, '/studentBooksForLib')
api.add_resource(DeleteStudent_book, '/studentBooks/<id>')
api.add_resource(Regions, '/region')
api.add_resource(Areas, '/area')
api.add_resource(Schools, '/school')
api.add_resource(Grades, '/grade')
api.add_resource(StudyYears, '/studyyaer')
api.add_resource(Subjects, '/subject')
api.add_resource(Publishers, '/publisher')
api.add_resource(Orders, '/order')
api.add_resource(ShipOrder, '/order/ship/<orderId>')
api.add_resource(Requests, '/request')
api.add_resource(ResendByLibrarian, '/resendrequest')
api.add_resource(Books, '/book')
api.add_resource(DirectorRequest, '/request/director/<requestId>')
api.add_resource(AreaRequest, '/request/area/<requestId>')
api.add_resource(RequestsByOrders, '/request/byorder')
api.add_resource(FinishRequest, '/request/<request_id>')
api.add_resource(sortedRequestedBooks, '/requestedbooks/<orderId>')
api.add_resource(Requestedbooks, '/requestedbooks')
api.add_resource(DeleteRequestedBook, '/delrequestedbooks/<id>')
api.add_resource(Formbooks, '/formbook')
api.add_resource(Seed, '/seed/<modelname>')
api.add_resource(Languages, '/language')
api.add_resource(Agregated, '/admin/grade')
api.add_resource(AgregatedRequest, '/admin/requests')
api.add_resource(Katos, '/kato')
api.add_resource(GetSubjectsPublisherLanguages, '/formbook/all')
api.add_resource(DeleteFormbook, '/formbook/<id>')
api.add_resource(DeleteGrade, '/grade/<id>')
api.add_resource(DeleteStudent, '/student/<id>')
api.add_resource(GetStudentCountSchool, '/school/students_count')
api.add_resource(SortByPublisher, '/order/<areaid>')
api.add_resource(Logs, '/logs')
api.add_resource(PaginateLog, '/logs/<number_page>/<number_rows>')



