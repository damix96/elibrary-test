def getSame(field, arr):
    res = []
    keys = []
    for key in arr:
        if key[0] == field:
            keys = [*key]
            keys.pop(0)
            res.append(keys)
    return res


def fieldsToArray(fields): 
    count = 0
    exclude = None    
    fields = fields.split()
    for item in fields:
        if item[0] == '-':
            print(exclude)
            if exclude == False:
                raise "Error!"
            exclude = True
            fields[count] = item[1:]
            count += 1
        else:
            if exclude:
                raise 'Er1'            
            exclude = False
    
    result = [item.split('.') for item in fields]
    return result, exclude
  

def convert(exclude,data,array):
    result = data.copy() if exclude else {}
    for key in array:
        if key[0] in data:
            val = data[key[0]]
            if isinstance(val, list) and len(key) > 1:                
                result[key[0]] = [convert(exclude,item,getSame(key[0],array)) for item in val]                
            elif type(val) is dict and len(key) > 1:
                result[key[0]] = convert(exclude,val, getSame(key[0], array))
            else:
                if exclude:
                    del result[key[0]]
                elif not exclude:
                    result[key[0]] = val    
    return result

def selectFields(data,fields):
    array, exclude = fieldsToArray(fields)    
    if isinstance(data, list):
        result = [convert(exclude,item,array) for item in data]
    elif isinstance(data, dict):            
        result = convert(exclude, data ,array)    
    else:
        result = data
    return result

def fieldsToArray2(fields): 
    index = None
    count = 0
    if isinstance(fields, list):
        return fields    
    fields = fields.split()
    result = [item.split('.') for item in fields]
    ress = []
    for item in result:
        ress.append(item)
        for i in item:
            if ']' in i and '[' in i:
                frame = i.index('[')
                indexes = ress[count].index(i)
                index = i[frame+1:-1]
                i = i[:3]
                ress[count][indexes] = i       
                ress[count].insert(indexes+1, index)
    count += 1        
    return ress        

def getDataByPath(data, str):
    array = fieldsToArray2(str)
    ArrIndex = 0
    if len(array) == 1:
        if array[0][0] in data:
            val = data[array[0][0]]
            if isinstance(val, list) and len(array[0]) > 1:            
                if val:
                    if (array[0][1].isdigit()):
                        if int(array[0][1]) < len(val):
                            ArrIndex = int(array[0][1])                    
                            array[0].pop(1)
                        else:
                            val = None                    
                    val = getDataByPath(val[ArrIndex],getSame(array[0][0],array))
            elif type(val) is dict and len(array[0]) > 1:
                val = getDataByPath(val, getSame(array[0][0], array))
            elif len(array[0]) > 1:
                val = None
        else:
            val = None
    else:
        val = None
    return val

# getdataByPath({ "a": [] }, 'a.b')
print(getDataByPath(
    {
        "rejected_by_director": None,        
        "requestedbooks":             
            {
                "requested_count": 3,
                "id": 5,
                "book_id": 9,
                "order_id": 1,
                "createdAt": 1559899704136.93,
                "book": {
                    "authors": "Энштэйн",
                    "subject_id": 1,
                    "name": "Математика-атамура 2018",
                    "active": None,
                    "language_id": 1,
                    "publisher_id": 1,
                    "year": 2018
                }
            }            
        ,
        "id": 3,
        "createdAt": 1559899704092.66        
    }, 'requestedbooks.book.publisher_id'))