""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Grade, editFields, GradeSchema, selectFields, Student, Requestedbook
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

types = ['director', 'librarian']

class Grades(Resource):

    @checkPermission(['admin', 'director', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        school_id = args.get('school_id')
        # if g.access['user']['type'] in types and school_id != g.access['user']['school_id']:
        #     return {'error': 'Доступ запрещён.'}, 403                    
        grades = Grade.query.filter_by(school_id=school_id).all()
        # for grade in grades:
        #     students = Student.query.filter_by(grad_id = grade['id'])
        # grades = selectFields(grades, 'grade grade_letter curator language_id language')
        # for student in students:
        #     grades.students_count += count
        #     count += 1
        return [GradeSchema().dump(item).data for item in grades]

    @checkPermission(['admin','moderator','librarian'])    
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            grades = Grade(**req)
            db.session.add(grades)
            db.session.commit()
            Log(g.access,'Создано класс', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий класса', 4)
            return { 'msg': 'Запрос не обработан' }, 500
   
    @checkPermission(['admin','moderator','librarian'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований класса', 5)                
                return {'error': 'Данные должны быть в формате JSON'}, 400
            grades = Grade.query.filter_by(id=req['id']).first()
            if not grades:
                Log(g.access,'Неудачная попытка редактирований класса', 5)
                return {'error': 'Такой пользователь не найден'}, 404
            
            ### Нужна проверка на то, является ли данная организация моей

            editFields(grades, req, ignore="")
            edited = GradeSchema().dump(grades).data
            db.session.commit()
            Log(g.access,'Редактировано класс', 2)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований класса', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
class DeleteGrade(Resource):
    @checkPermission(['admin','librarian'])    
    def delete(self, id):
        try:
            data = Grade.query.filter_by(id=id).first()
            students = Student.query.filter_by(grad_id=data.id).first()
            if students:
                Log(g.access,'Неудачная попытка удалений класса', 5)
                return {'error': 'удаление невозможно, класс имеет студентов'}, 405
            requestedbook = Requestedbook.query.filter_by(grade_id=data.id).first()
            if requestedbook:
                Log(g.access,'Неудачная попытка удалений класса', 5)
                return {'error': 'удаление невозможно, класу превязан запрошенные книги'}, 405
            db.session.delete(data)
            db.session.commit()
            Log(g.access,'Удалено класс', 3)
            return "deleted"
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при удалений класса', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class Agregated(Resource):

    @checkPermission(['admin','librarian','moderator', 'director'])
    def get(self):
        """Get specific info"""
        args = request.args
        school_id = args.get('school_id')
        grades = Grade.query.filter_by(school_id=school_id).all()
        grades = [selectFields(GradeSchema().dump(item).data, 'id grade grade_letter curator language_id language') for item in grades]
        for grade in grades:
            grade['students_count'] = Student.query.filter_by(grad_id = grade['id']).count()
        return grades