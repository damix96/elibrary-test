import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Publisher, editFields, PublisherSchema, selectFields, User, UserSchema
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class Publishers(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select'] 
        publishers = Publisher.query.all()
        result = [PublisherSchema().dump(item).data for item in publishers]
        for publisher in result:
            # print(publisher)
            user = User.query.filter_by(publisher_id=publisher['id']).first()
            user = UserSchema().dump(user).data
            # school = School.query.filter_by(publisher_id=publisher['id']).count()
            # publisher['school_count'] = school
            publisher['user'] = user
        if select: 
            result = selectFields(result, select)
        return result

    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            publisher = Publisher(**req)
            db.session.add(publisher)
            db.session.commit()
            Log(g.access,'Создано издательство', 1)
            return PublisherSchema().dump(publisher).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий издательство', 4)
            return { 'msg': 'Запрос не обработан' }, 500


    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:        
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований издательство', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            publisher = Publisher.query.filter_by(id=req['id']).first()
            if not publisher:
                Log(g.access,'Неудачная попытка редактирований издательство', 5)
                return {'error': 'Такой пользователь не найден'}, 404
            
            ### Нужна проверка на то, является ли данная организация моей

            editFields(publisher, req, ignore="")
            edited = PublisherSchema().dump(publisher).data
            db.session.commit()
            Log(g.access,'Редактировано издательство', 1)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований издательство', 4)
            return { 'msg': 'Запрос не обработан' }, 500
        
