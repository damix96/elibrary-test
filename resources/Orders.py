"""
Order status:
1 - Новый
2 - Отгружен
3 - получено школой
"""
import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, selectFields, RequestSchema, Request, Requestedbook, RequestedbookSchema, Order, OrderSchema, School, SchoolSchema
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

def geta(target={}, path=''):
    path = path.split('.')
    result = target
    for key in path:
        if not key in result:
            return None
        result = result.get(key)
    return result

class Orders(Resource):
    """CRUD"""
    @checkPermission(['publisher', 'admin', 'moderator', 'region'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select'] 
        # data = Order.query.filter_by(publisher_id=g.access['user']['publisher_id'], **args).all()        
        if args:
            data = Order.query.filter_by(**args).all()
        else:
            data = Order.query.all()
        # """Логирование"""
        # for dataOne in data:
        #     if g.access['user']['type'] in 'publisher' and dataOne.publisher_id != g.access['user']['publisher_id']:
        #         return {'error': 'Доступ запрещён.'}, 403  
        # """--------------------------------------"""
        result = [OrderSchema().dump(item).data for item in data]
        if select: 
            result = selectFields(result, select)
        return result

    @checkPermission(['area'])
    def post(self):
        """Create new Order as area"""
        try:
            isAllRequest = True
            area_id = g.access['user']['area_id'] or request.get_json(force=True)['area_id']
            studyyear_id = request.get_json(force=True)['studyyear_id']
            if not area_id and studyyear_id:
                Log(g.access,'Неудачная попытка созданий запроса в издательство', 5)
                raise "Error: Параметры не переданы"
            requests = Request.query.filter_by(area_id=area_id, status=4,studyyear_id = studyyear_id).all()
            schools = School.query.filter_by(area_id=area_id).all()
            if len(requests) == 0:
                Log(g.access,'Неудачная попытка созданий запроса в издательство', 5)
                return { "msg": "Нет заявок." }, 400
            if len(schools) != len(requests):
                Log(g.access,'Неудачная попытка созданий запроса в издательство', 5)
                return { "error": "Не все заявки завершены" }, 405
            ord = {}
            
            # print([RequestSchema().dump(item).data for item in requests])
            for req in requests:
                for requestedbook in req.requestedbooks:
                    pub_id = requestedbook.book.publisher_id
                    ord[str(pub_id)] = ord.get(str(pub_id)) or { 'requested_count': 0 }
                    ord[str(pub_id)]['requested_count'] += requestedbook.requested_count
                    ord[str(pub_id)]['publisher_id'] = pub_id
                    ord[str(pub_id)]['status'] = 1 
                    ord[str(pub_id)]['createdAt'] = time.time() * 1000
                    ord[str(pub_id)]['area_id'] = area_id
                    ord[str(pub_id)]['studyyear_id'] = studyyear_id
                    # ord[pub_id].requests = ord[pub_id].requests or []
                req.status = 5
                db.session.add(req)
            print(ord)
            orders = [Order(**order) for order in ord.values()]
            for order in orders:
                db.session.add(order)
            db.session.commit()
            for req in requests:
                for requestedbook in req.requestedbooks:
                    pub_id = requestedbook.book.publisher_id
                    for order in orders:
                        if order.publisher_id == pub_id:
                            requestedbook.order_id = order.id
                            req.order_id = order.id
                            break
                    db.session.add(requestedbook)
            db.session.commit()
            Log(g.access,'Создан запрос в издательство', 1)
            return {"orders": OrderSchema().dump(orders, many=True).data}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий запроса в издательство', 4)
            return { 'msg': 'Запрос не обработан' }, 500
        # for order in orders
    
    # @checkPermission(['admin'])
    # def delete(self):
    #     req = request.get_json(force=True)
    #     id = req['id']
    #     data = Order.query.filter_by(id=id).delete()
    #     # db.session.delete(data)
    #     db.session.commit()
    #     return "deleted"

class SortByPublisher(Resource):
    @checkPermission(['admin', 'moderator', 'area', 'region'])
    def get(self, areaid):
        orders = Order.query.filter_by(area_id = areaid).all()
        orders = [OrderSchema().dump(item).data for item in orders]
        resultBypublisher = {}
        for order in orders:        
            order['publisher'] = selectFields(order['publisher'], 'id name_rus name_kaz')
            index = str(order['publisher_id'])
            resultBypublisher[index] = resultBypublisher.get(index) or {'requested_count': 0, 'shipped_count': 0}
            resultBypublisher[index]['publisher'] = order['publisher']
            resultBypublisher[index]['requested_count'] += order['requested_count'] or 0
            requestedbook = Requestedbook.query.filter_by(order_id=order['id']).all()
            for reqbook in requestedbook:
                resultBypublisher[index]['shipped_count'] += reqbook.shipped_count or 0
            resultBypublisher[index]['createdAt'] = order['createdAt']
            resultBypublisher[index]['status'] = order['status']
            resultBypublisher[index]['id'] = order['id']

        resultBypublisher = { "data": [item for item in resultBypublisher.values()] }
        return resultBypublisher 

class ShipOrder(Resource):
    """Ship Order"""
    @checkPermission(['publisher'])
    def post(self, orderId):
        # req = request.get_json(force=True)
        order = Order.query.filter_by(id=orderId).first()
        userType = geta(g.access, 'user.type')
        if not userType == 'publisher':
            return { 'data': 'Нет прав.' }, 403
        order.status = 2
        requestedbooks = Requestedbook.query.filter_by(order_id=order.id).all()
        for rbook in requestedbooks:
            order.shipped_count = (rbook.shipped_count or 0) + (order.shipped_count or 0) 
            
        db.session.add(order)
        db.session.commit()
        return OrderSchema().dump(order).data
        
# class DeclineRequest(Resource):
#     """Decline Request"""
#     def post(self, requestId):
#         request = Request.query.filter_by(id=requestId).first()
#         request.status -= 1
#         request.confirmedAt = time.time() * 1000
#         db.session.commit()
#         return RequestSchema().dump(request).data
