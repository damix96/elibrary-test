import uuid
import time
import pprint
from urllib.request import urlopen
from flask import request, jsonify, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Access, User, UserSchema, AccessSchema
from resources.Logs import Log
import re
import json


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


def json(data, schema): return schema.dump(data).data


def error(msg, status):
    return {'error': msg}, status

def auth():
    """Authorization"""
    try:
        data = request.get_json(force=True)
        user = User.query.filter_by(login=data['login']).first()
        if (not user):
            # Log(g.access,'Неудачная попытка входа', 5)
            return error('Неверный логин или пароль', 403)
        if not User.check_password(user, data['password']):
            # Log(g.access,'Неудачная попытка входа', 5)
            return error('Неверный логин или пароль', 403)
        token = str(uuid.uuid4())
        # print(format(ipData['ip']))
        if request.headers.getlist("X-Forwarded-For"):
            ipinf = request.headers.getlist("X-Forwarded-For")[0]
        else:
            ipinf = request.remote_addr
        access = Access(
            user_id=user.id,
            token=token,
            os=ipInfo.os,
            # ip=ipInfo.ipaddress,        
            ip = ipinf,
            browser=ipInfo.browser,
            createdAt=time.time() * 1000
        )
        db.session.add(access)    
        access2 = AccessSchema().dump(access).data
        db.session.commit()        
        Log(access2,'Выполнен вход', 5)
        return {"token": token}, 200
    except:
        db.session.rollback()
        db.session.flush()
        Log(access2,'Неудачная попытка входа', 5)
        return { 'msg': 'Запрос не обработан' }, 500

def info():
    """ Get info about current user by token """

    data = request.args #get_json(force=True)
    fields = ['user']
    if ("fields" in data):
        fields = list(data['fields'].split())
    
    if (not 'token' in request.headers):
        return error('Ошибка авторизации', 403)
    access = Access.query.filter_by(token=request.headers['token']).first()
    if (not access):
        return error('Ошибка авторизации', 403)
    access = AccessSchema().dump(access).data
    return access['user']

def checkPermission(privileges=[]):
    """ Access checker Middleware """
    def decor(func):
        def wrapperFunc(*args, **kwargs):
            if not ('token' in request.headers):
                return error('Доступ запрещён.', 403)
            access = Access.query.filter_by(token=request.headers['token']).first()
            if not access:
                return error('Доступ запрещён.', 403)
            access = AccessSchema().dump(access).data
            if not ('user' in access and (access['user']['type'] in privileges) if len(privileges) > 0 else True):
                return error('Доступ запрещён.', 403)
            g.access = access
            return func(*args, **kwargs)
        return wrapperFunc
    return decor

# def checkGetRequest(typeData, data, user, parametrs):
#     typeOfData = ['School', 'Grade', 'Student', 'Requestedbook', 'Request', 'Students_Book', 'Order', 'Region', 'Area']
#     for types in typeOfData:
#         if types != typeData:
#             return False
#     # data = typeData.query.filter_by()

# def checkPutRequest(typeData, data, user, parametrs):
#     typeOfData = ['schools', 'grades', 'students', 'requestedbooks', 'requests', 'students_books', 'orders', 'regions', 'areas']



class Auth(Resource):
    def post(self):
        return auth()

    def get(self):
        return info()
        # json_data = request.get_json(force=True)
        # if not json_data:
        #        return {'message': 'No input data provided'}, 400
        # # Validate and deserialize input
        # data, errors = category_schema.load(json_data)
        # if errors:
        #     return errors, 422
        # category = Category.query.filter_by(name=data['name']).first()
        # if category:
        #     return {'message': 'Category already exists'}, 400
        # category = Category(
        #     name=json_data['name']
        #     )

        # db.session.add(category)
        # db.session.commit()

        # result = category_schema.dump(category).data

        # return { "status": 'success', 'data': result }, 201
    # def put(self):
    #     json_data = request.get_json(force=True)
    #     if not json_data:
    #            return {'message': 'No input data provided'}, 400
    #     # Validate and deserialize input
    #     data, errors = category_schema.load(json_data)
    #     if errors:
    #         return errors, 422
    #     category = Category.query.filter_by(id=data['id']).first()
    #     if not category:
    #         return {'message': 'Category does not exist'}, 400
    #     category.name = data['name']
    #     db.session.commit()

    #     result = category_schema.dump(category).data

    #     return { "status": 'success', 'data': result }, 204
    # def delete(self):
    #     json_data = request.get_json()
    #     # return json_data
    #     if not json_data.get('id'):
    #            return {'message': 'No input data provided'}, 400
    #     # Validate and deserialize input
    #     # data, errors = category_schema.load(json_data)
    #     # if errors:
    #     #     return errors, 422
    #     category = Category.query.filter_by(id=json_data['id']).delete()
    #     db.session.commit()
    #     result = category_schema.dump(category).data
    #     return { "status": 'success', 'data': result }, 200
