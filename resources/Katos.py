""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Grade, editFields, SchoolSchema, selectFields, Kato, KatoSchema
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()



class Katos(Resource):

    @checkPermission(['admin', 'area','moderator', 'region'])
    def get(self):
        """Get specific info"""
        args = request.args
        area_id = args.get('area_id')
        if area_id:
            # del args['area_id']
            kato = Kato.query.filter_by(area_id=area_id).all()
        else:
            kato = Kato.query.all()
        return [KatoSchema().dump(item).data for item in kato]

    @checkPermission(['admin','moderator'])    
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            kato = Kato(**req)
            db.session.add(kato)
            db.session.commit()
            Log(g.access,'Создано като', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий като', 4)
            return { 'msg': 'Запрос не обработан' }, 500
   
    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований като', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            kato = Kato.query.filter_by(id=req['id']).first()
            if not kato:
                Log(g.access,'Неудачная попытка редактирований като', 5)
                return {'error': 'Такой като не найден'}, 404        
            ### Нужна проверка на то, является ли данная организация моей
            editFields(kato, req, ignore="")
            edited = KatoSchema().dump(kato).data
            Log(g.access,'Редактировано като', 2)
            db.session.commit()
            return { 'data': edited }    
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований като', 4)
            return { 'msg': 'Запрос не обработан' }, 500