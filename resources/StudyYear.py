""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, StudyYear, StudyYearSchema, editFields, GradeSchema
from resources.Auth import checkPermission
from resources.Logs import Log


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class StudyYears(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        studytear = StudyYear.query.all()
        return [StudyYearSchema().dump(item).data for item in studytear]

    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            studyyearss = StudyYear.query.filter_by(current=True).first()
            if studyyearss and req['current'] == True:
                studyyearss.current = False
                db.session.commit()
            studyyears = StudyYear(**req)        
            db.session.add(studyyears)
            db.session.commit()
            Log(g.access,'Создан учебный год', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий учебного года', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin','moderator'])
    def put(self):
        try:            
            req = request.get_json(force=True)
            studyyearss = StudyYear.query.filter_by(current=True).first()
            if studyyearss and req['current'] == True:
                studyyearss.current = False
                db.session.commit()
            id = req['id']
            data = StudyYear.query.filter_by(id=id).first()
            editFields(data, req)
            db.session.commit()
            Log(g.access,'Редактирован учебный год', 2)
            return StudyYearSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований учебного года', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    # @checkPermission(['admin'])
    # def delete(self):
    #     req = request.get_json(force=True)
    #     id = req['id']
    #     data = StudyYear.query.filter_by(id=id).first()
    #     db.session.delete(data)
    #     db.session.commit()
    #     return "deleted"
   
