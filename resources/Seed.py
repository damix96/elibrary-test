import time
import pprint
import Model as Models
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, selectFields, LanguageSchema, Language
from resources.Auth import checkPermission

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

mdl = lambda name: getattr(Models, name)

class Seed(Resource):
    """CRUD"""
    @checkPermission(['admin'])
    def get(self, modelname):
        """Get specific info"""
        args = {**request.args}
        data = mdl(modelname).query.filter_by(**args).all()
        pp(data)
        return [mdl(modelname + 'Schema')().dump(item).data for item in data]

    @checkPermission(['admin'])
    def post(self, modelname):
        """ create """
        req = request.get_json(force=True)
        data = mdl(modelname)(**req)
        db.session.add(data)
        db.session.commit()
        return mdl(modelname + 'Schema')().dump(data).data, 200

    @checkPermission(['admin'])
    def delete(self, modelname):
        args = {**request.args}
        data = mdl(modelname).query.filter_by(**args).delete()
        db.session.commit()
        return {"Success": "Успех! Ура! Да здравствует Нурбек!"}, 200

    @checkPermission(['admin'])
    def put(self, modelname):
        """Edit"""
        req = request.get_json(force=True) or {}
        args = {**request.args}
        data = mdl(modelname).query.filter_by(**args).first()
        editFields(data, req, ignore="")
        edited = mdl(modelname + 'Schema')().dump(data).data
        db.session.commit()
        return edited
