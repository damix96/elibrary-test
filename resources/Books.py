import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, BookSchema, Book, selectFields
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

class Books(Resource):
    """CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args} 
        select = args.get('select')
        if select:
            del args['select']
        data = Book.query.filter_by(**args).all()
        result = [BookSchema().dump(item).data for item in data]
        if select: 
            result = selectFields(result, select)
        return result

    @checkPermission(['admin','moderator'])    
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            book = Book(**req)
            db.session.add(book)        
            db.session.commit()
            Log(g.access,'Создано книга', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий книги', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований книги', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            book = Book.query.filter_by(id=req['id']).first()
            if not book:
                Log(g.access,'Неудачная попытка редактирований книги', 5)
                return {'error': 'Такой пользователь не найден'}, 404
        
            editFields(book, req, ignore="")
            edited = BookSchema().dump(book).data
            db.session.commit()
            Log(g.access,'Редактировано книга', 2)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований книги', 4)
            return { 'msg': 'Запрос не обработан' }, 500

