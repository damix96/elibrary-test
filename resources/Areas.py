""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Area, editFields, AreaSchema, selectFields, School, User, UserSchema
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class Areas(Resource):

    @checkPermission(['admin', 'area', 'moderator', 'region'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select']    
        areas = Area.query.all()
        result = [AreaSchema().dump(item).data for item in areas]
        # resultByschool = {result, 'school_count': 0}
        for area in result:
            # print(area)
            user = User.query.filter_by(area_id=area['id']).first()
            user = UserSchema().dump(user).data
            # user = selectFields(user, '-login')
            school = School.query.filter_by(area_id=area['id']).count()
            area['school_count'] = school
            area['user'] = user
        if select: 
            result = selectFields(result, select)
        return result


    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            area = Area(**req)
            db.session.add(area)
            db.session.commit()
            Log(g.access,'Создано область', 1)
            return AreaSchema().dump(area).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий областя', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    # def delete(self):
    #     req = request.get_json(force=True)
    #     area = Area.query.filter_by(id=req['id']).delete()
    #     db.session.commit()
    #     return { 'data': 'Успех!' }, 200

    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            area = Area.query.filter_by(id=req['id']).first()
            if not area:
                Log(g.access,'Неудачная попытка редактирований', 5)
                return {'error': 'Такой район не найден'}, 404
            
            ### Нужна проверка на то, является ли данная организация моей

            editFields(area, req, ignore="region region_id")
            edited = AreaSchema().dump(area).data
            db.session.commit()
            Log(g.access,'Редактировано область', 2)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований областя', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    # @checkPermission(['admin'])
    # def delete(self):
    #     req = request.get_json(force=True)
    #     id = req['id']
    #     data = Area.query.filter_by(id=id).first()
    #     db.session.delete(data)
    #     db.session.commit()
    #     return "deleted"
