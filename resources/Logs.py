""" Модуль Логирование """
"""
typeLog

1 - create
2 - edit
3 - delete
4 - error
5 - info
"""

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Logging, editFields, selectFields, LogSchema, User

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

def Log(access, action, typeLog):
        """ create """
        login = None
        userType = None
        if not access['user']:
            user = User.query.filter_by(id=access['user_id']).first()
            login = user.login
            userType = user.type
        else:
            login = access['user']['login']
            userType = access['user']['type']
        result = {
            "userLogin": login,
            "userType": userType,
            "ip": access['ip'],
            "os": access['os'],
            "browser": access['browser'],
            "createdAt": time.time()*1000,
            "action": action,
            "typeLog": typeLog
        }        
        log = Logging(**result)
        db.session.add(log)
        db.session.commit()
        # return { 'data': 'Логинг Успех!' }, 200 

   
             