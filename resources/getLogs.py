from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Logging, editFields, selectFields, LogSchema
from resources.Auth import checkPermission

class Logs(Resource):

    @checkPermission(['admin','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        # if not args['number_page'] and not args['number_rows']:
            # return {'msg': 'Параметры не переданы'}, 405
        dateFrom = args.get('dateFrom')
        dateTo = args.get('dateTo')
        
        if dateFrom and dateTo:
            del args['dateFrom']
            del args['dateTo']
            count = Logging.query.filter_by(**args).filter(Logging.createdAt.between(dateFrom, dateTo)).count()
        else:            
            count = Logging.query.filter_by(**args).count()

        # count = Logging.query.limit(int(args['number_rows'])).offset(int(args['number_page']) * int(args['number_rows']))
        return count

class PaginateLog(Resource):
    @checkPermission(['admin','moderator'])
    def get(self, number_page, number_rows):
        """Get specific info"""
        args = {**request.args}
        # logs = Logging.query.filter_by(**args).all()
        # logs = Logging.query.limit(int(number_rows)).offset(int(number_page) * int(number_rows))
        dateFrom = args.get('dateFrom')
        dateTo = args.get('dateTo')

        if dateFrom and dateTo:
            del args['dateFrom']
            del args['dateTo']
            logs = Logging.query.filter_by(**args).filter(Logging.createdAt.between(dateFrom, dateTo)).order_by(Logging.id.desc()).paginate(int(number_page), int(number_rows), False)
        else:
            logs = Logging.query.filter_by(**args).order_by(Logging.id.desc()).paginate(int(number_page), int(number_rows), False)
        total = logs.total
        log_items = logs.items
        # print(total, ' total ')        
        return [LogSchema().dump(item).data for item in log_items] 
    
    
