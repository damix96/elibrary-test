import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Region, editFields, RegionSchema, selectFields, User, UserSchema
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class Regions(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select']        
        regions = Region.query.all()
        result = [RegionSchema().dump(item).data for item in regions]
        for region in result:
            # print(area)
            user = User.query.filter_by(region_id=region['id']).first()
            user = UserSchema().dump(user).data
            # user = selectFields(user, '-login')
            region['user'] = user
        if select:
            result = selectFields(result, select)
        return result

    @checkPermission(['admin', 'moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            region = Region(**req)
            db.session.add(region)
            db.session.commit()
            Log(g.access,'Создано область', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий области', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований области', 4)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            region = Region.query.filter_by(id=req['id']).first()
            if not region:
                Log(g.access,'Неудачная попытка редактирований области', 4)
                return {'error': 'Такой пользователь не найден'}, 404
            
            ### Нужна проверка на то, является ли данная организация моей

            editFields(region, req, ignore="")
            edited = RegionSchema().dump(region).data
            db.session.commit()
            Log(g.access,'Редактировано область', 2)
            return edited
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований области', 4)
            return { 'msg': 'Запрос не обработан' }, 500
