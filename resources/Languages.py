import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, LanguageSchema, Language, editFields
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

class Languages(Resource):
    """CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        languages = Language.query.all()
        return [LanguageSchema().dump(item).data for item in languages]

    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            language = Language(**req)
            db.session.add(language)
            db.session.commit()
            Log(g.access,'Создан язык', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий языка', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    
    @checkPermission(['admin','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований языка', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            language = Language.query.filter_by(id=req['id']).first()
            if not language:
                Log(g.access,'Неудачная попытка редактирований языка', 5)
                return {'error': 'Такой язык не найден'}, 404            
            ### Нужна проверка на то, является ли данная организация моей
            editFields(language, req, ignore="")
            edited = LanguageSchema().dump(language).data
            db.session.commit()
            Log(g.access,'Редактировано язык', 2)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований языка', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    

    # @checkPermission(['admin', 'school'])
    # def put(self):
    #     """Edit"""
    #     req = request.get_json(force=True)
    #     if not req:
    #         return {'error': 'Данные должны быть в формате JSON'}, 400
    #     school = School.query.filter_by(id=req['id']).first()
    #     if not school:
    #         return {'error': 'Такой пользователь не найден'}, 404
        
    #     ### Нужна проверка на то, является ли данная организация моей

    #    gnore="")
    #     edited = LanguageschemLanguages().dump(school).data
    #     db.session.commit()
    #     return { 'data': edited }
