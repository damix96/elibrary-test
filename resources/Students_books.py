""" Модуль Студент-книги """
"""
Status 1 - Выданные 
Status 2 - Возврат
"""

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Subject, editFields, Students_BookSchema, Students_Book, Request, selectFields, Student, Formbook, FormbookSchema, Grade
from resources.Auth import checkPermission
from resources.Logs import Log


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()
 

class Student_books(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        student_id = args.get('student_id')
        school_id = args.get('school_id')
        grade_id = args.get('grade_id')      
        select = args.get('select')
        if select: 
            del args['select']
        query = Students_Book.query
        
        if student_id:
            query = query.filter_by(student_id=student_id)       
        if grade_id:
            query = query.filter_by(grade_id=grade_id)
            
        student_book = query.all()
        
        result = [Students_BookSchema().dump(item).data for item in student_book]     
        
        if select:
            result = selectFields(result,select)
        return result

    @checkPermission(['librarian'])
    def put(self):
        try:
            req = request.get_json(force=True)
            id = req['id']
            # Проверка мой ли учебник

            data = Students_Book.query.filter_by(id=id).first()
            editFields(data, req, ignore='grade book request')
            db.session.commit()
            Log(g.access,'Редактировано список книг ученика', 2)
            return Students_BookSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований список книг ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin', 'librarian', 'moderator'])    
    def post(self):
        """ create """
        try:            
            req = request.get_json(force=True)
            data = Students_Book(**req)
            db.session.add(data)
            db.session.commit()
            Log(g.access,'Создано список книг ученика', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий список книг ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500
        
class DeleteStudent_book(Resource):
    @checkPermission(['admin', 'librarian'])
    def delete(self, id):
        try:
            data = Students_Book.query.filter_by(id=id, school_id=g.access['user']['school_id']).first()
            db.session.delete(data)
            db.session.commit()
            Log(g.access,'Удалено список книг ученика', 3)
            return "deleted"        
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при удалений список книг ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class GetBook(Resource):
    @checkPermission(['librarian'])      
    def get(self):
        """Get specific info"""
        args = request.args
        school_id = args.get('school_id')
        grade_id = args.get('grade_id')
        grade = args.get('grade')
        language_id = args.get('language_id')
        select = args.get('select')
        if select: 
            del args['select']
        query = Students_Book.query    
        if grade_id and grade and language_id:
            query = query.filter_by(grade_id=grade_id)
            books = Formbook.query.filter_by(grade=grade, language_id=language_id).all()
            books_res = [FormbookSchema().dump(item).data for item in books]
            
        student_book = query.all()
        gradeData = Grade.query.filter_by(id=grade_id).first()

        """Проверка на доступ"""
        if gradeData.school_id != g.access['user']['school_id']:
            return {'error': 'Доступ запрещён.'}, 403
        """------------------------------------------"""
        result = [Students_BookSchema().dump(item).data for item in student_book]
        plan_getbook = Student.query.filter_by(grad_id=grade_id).count()
        flipp = []
        ress = []
        if select:
            result = selectFields(result,select)
        if grade_id and grade:                  
            for stud_book in result:
                for book in books_res:       
                    book['plan_getbook'] = plan_getbook or 0
                    book['return_book'] = 0
                    book['getted_book'] = 0                       
                    if book['book_id'] == stud_book['book_id']:                        
                        if stud_book['status'] == str(2):
                            stud_book['return_book'] = stud_book.get('return_book', 0) + 1
                        else:
                            stud_book['getted_book'] = stud_book.get('getted_book', 0) + 1
                
        
        # ress = [x for i, x in enumerate(result) if x not in result[:i]]    
        for res in books_res:
            if res['book_id'] not in flipp:                
                flipp.append(res['book_id'])                
                ress.append(res)

        for res in ress:
            for resul in result:
                if res['book_id'] == resul['book_id']:                    
                    res['return_book'] = res.get('return_book', 0) + resul.get('return_book', 0)
                    res['getted_book'] = res.get('getted_book', 0) + resul.get('getted_book', 0)

        return ress
