import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, selectFields, FormbookSchema, Formbook, Subject, SubjectSchema, Publisher, PublisherSchema, Language, LanguageSchema, Book
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

class Formbooks(Resource):
    """CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select']
        data = Formbook.query.filter_by(**args).all()
        result = [FormbookSchema().dump(item).data for item in data]
        if select:
            result = selectFields(result, select)
        return result

    @checkPermission(['admin','moderator'])    
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            book_id = req['book_id']
            book = Book.query.filter_by(id=book_id).first()
            req['language_id'] = book.language_id
            data = Formbook(**req)
            db.session.add(data)
            db.session.commit()
            Log(g.access,'Создано форма 400', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий форма 400', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin','moderator'])
    def put(self):
        try:
            req = request.get_json(force=True)
            id = req['id']
            data = Formbook.query.filter_by(id=id).first()
            editFields(data, req)
            db.session.commit()
            Log(g.access,'Редактировано форма 400', 2)
            return FormbookSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований форма 400', 4)
            return { 'msg': 'Запрос не обработан' }, 500


class DeleteFormbook(Resource):
    @checkPermission(['admin','moderator'])    
    def delete(self, id):
        try:
            data = Formbook.query.filter_by(id=id).first()
            db.session.delete(data)
            db.session.commit()
            Log(g.access,'Удалено форма 400', 3)
            return "deleted"
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при удалений форма 400', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class GetSubjectsPublisherLanguages(Resource):
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        result = {}
        result = result.get(0)
        subject = Subject.query.all()
        subject = [SubjectSchema().dump(item).data for item in subject]
        # result = {'subjects': subject}
        publisher = Publisher.query.all()
        publisher = [PublisherSchema().dump(item).data for item in publisher]            

        # language = Language.query.all()
        # language = [LanguageSchema().dump(item).data for item in language]

        result = {'subjects': subject}
        result.update({'publisher': publisher})
        # result.update({'language': language})

        # result['subject'] = subject
        # result['publisher'] = publisher
        # result['language'] = language
        # for sub in subject:
            # result['subjectall'] += sub


        return result