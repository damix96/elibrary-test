""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Student, editFields, StudentSchema, selectFields, Students_Book, Students_BookSchema, Formbook, Grade
from resources.Auth import checkPermission
from resources.Logs import Log


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class Students(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        grade_id = args.get('grade_id')
        grade = args.get('grade')
        language_id = args.get('language_id')
        select = args.get('select')
        if select:
            del args['select']
        students = Student.query.filter_by(grad_id=grade_id).all()
        result = [StudentSchema().dump(item).data for item in students]
        # if grade:
        #     del args['grade']
        for student in result:  
            student['plan_getbook'] = Formbook.query.filter_by(grade=grade, language_id=language_id).count()
            if grade and language_id:
                student['getted_book'] = Students_Book.query.filter_by(student_id=student['id']).count()
            student['return_book'] = Students_Book.query.filter_by(student_id=student['id'], status='2').count()
        
        if select:
            result = selectFields(result,select)
        return result

    @checkPermission(['admin', 'librarian','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            grade_id = req['grad_id']
            grade = Grade.query.filter_by(id=grade_id).first()
            req['school_id'] = grade.school_id
            student = Student(**req)
            db.session.add(student)
            db.session.commit()
            Log(g.access,'Создано ученик', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    @checkPermission(['librarian','moderator','admin'])
    def put(self):
        try:
            req = request.get_json(force=True)
            id = req['id']
            # Проверка мой ли учебник
            data = Student.query.filter_by(id=id).first()
            editFields(data, req)
            db.session.commit()
            Log(g.access,'Редактировано ученик', 2)
            return StudentSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
class DeleteStudent(Resource):
    @checkPermission(['admin','librarian'])   
    def delete(self, id):
        try:
            data = Student.query.filter_by(id=id).first()
            studentbook = Students_Book.query.filter_by(student_id=id).all()
            for stud_book in studentbook:
                if stud_book.status == '1':
                    Log(g.access,'Неудачная попытка удалений ученика', 5)
                    return {'error': 'У студента есть не возвращенные книги'}, 405
            Students_Book.query.filter_by(student_id=id).delete()                
            db.session.delete(data)
            db.session.commit()
            Log(g.access,'Удалено ученик', 3)
            return "deleted"
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при удалений ученика', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class AgregatedStudent(Resource):
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        grade_id = args.get('grade_id')
        students = Student.query.filter_by(grad_id=grade_id).all()
        students = [selectFields(StudentSchema().dump(item).data, 'id firstname lastname  fathername iin') for item in students]
        return students