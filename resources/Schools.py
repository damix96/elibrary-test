import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Area, School, editFields, SchoolSchema,selectFields, User, UserSchema, Grade, GradeSchema, Student
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

class Schools(Resource):
    """SCHOOLS CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        area_id = args.get('area_id')
        search = args.get('search') or ""
        select = args.get('select')
        if select:
            del args['select']
            
        conditions = [
            School.name_rus.like(search), 
            School.name_kaz.like(search)
            ]
        query = School.query
        
        if area_id:
            query = query.filter_by(area_id=area_id)
        if len(search) > 0:
            query = query.filter(db.or_(*conditions))

        schools = query.all()
        schools = [SchoolSchema().dump(item).data for item in schools]
        for school in schools:
            user = User.query.filter_by(school_id=school['id']).first()
            user = UserSchema().dump(user).data            
            user = selectFields(user, '-school')
            school['user'] = user
            # grades = Grade.query.filter_by(school_id=school['id']).all()
            # grades = [selectFields(GradeSchema().dump(item).data, 'id') for item in grades]
            # for grade in grades:
            #     school['students_count'] = Student.query.filter_by(grad_id = grade['id']).count()
        # result = [SchoolSchema().dump(item).data for item in schools]

        if select:
            schools = selectFields(schools,select)

        return schools

    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            school = School(**req)
            db.session.add(school)
            db.session.commit()
            Log(g.access,'Создано школа', 1)
            return SchoolSchema().dump(school).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий школы', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    # @checkPermission(['admin'])
    # def delete(self):
    #     req = request.get_json(force=True)
    #     school = School.query.filter_by(id=req['id']).delete()
    #     db.session.commit()
    #     return { 'data': 'Успех!' }, 200

    @checkPermission(['admin', 'director','moderator'])
    def put(self):
        """Edit"""
        try:
            req = request.get_json(force=True)
            if not req:
                Log(g.access,'Неудачная попытка редактирований школы', 5)
                return {'error': 'Данные должны быть в формате JSON'}, 400
            school = School.query.filter_by(id=req['id']).first()
            if not school:
                Log(g.access,'Неудачная попытка редактирований школы', 5)
                return {'error': 'Такой пользователь не найден'}, 404
            
            ### Нужна проверка на то, является ли данная организация моей

            editFields(school, req, ignore="")
            edited = SchoolSchema().dump(school).data
            db.session.commit()
            Log(g.access,'Редактировано школа', 2)
            return { 'data': edited }
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований школы', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    
class GetStudentCountSchool(Resource):
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = request.args
        area_id = args.get('area_id')
        search = args.get('search') or ""
        select = args.get('select')
        count = 0
        if select:
            del args['select']
            
        conditions = [
            School.name_rus.like(search), 
            School.name_kaz.like(search)
            ]
        query = School.query
        
        if area_id:
            query = query.filter_by(area_id=area_id)
        if len(search) > 0:
            query = query.filter(db.or_(*conditions))

        schools = query.all()
        schools = [SchoolSchema().dump(item).data for item in schools]
        for school in schools:
            user = User.query.filter_by(school_id=school['id']).all()
            user = [UserSchema().dump(item).data for item in user]
            user = selectFields(user, '-school')
            for us in user:
                if us['type'] == 'director':            
                    school['director'] = us
                elif us['type'] == 'librarian':
                    school['librarian'] = us
            # school['user'] = user            
            school['students_count'] = Student.query.filter_by(school_id = school['id']).count()
        # result = [SchoolSchema().dump(item).data for item in schools]

        if select:
            schools = selectFields(schools,select)

        return schools
