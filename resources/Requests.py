"""
Request status:
1 - library
2 - director
3 - area
4 - confirmed by area
5 - Order created
6 - Отправлено в школу

"""
import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, selectFields, RequestSchema, Request, Requestedbook, RequestedbookSchema, Order, getDataByPath, School, OrderSchema, Student, Grade, StudyYear
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

def geta(target={}, path=''):
    path = path.split('.')
    result = target
    for key in path:
        if not key in result:
            return None
        result = result.get(key)
    return result

def setRequestId(item):
    item.request_id=request.id
    return item

class RequestsByOrders(Resource):
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian'])
    def post(self):
        req = request.get_json(force=True)
        data = Request.session.filter(Request.id.in_(req['orders'])).all()
        return { 'data': [RequestSchema().dump(item).data for item in data] }

class Requests(Resource):
    """CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}        
        select = args.get('select')
        id = args.get('id')
        school_id = args.get('id')
        publisher_id = args.get('id')
        if select:
            del args['select']

        # if id and school_id and publisher_id:
        #     data = Request.query.filter_by(id=id, school_id=school_id).all()
        # else:        
        data = Request.query.filter_by(**args).all()

        """Проверка на доступ"""
        if g.access['user']['type'] in ['director', 'librarian']:
            for dataOne in data:
                if dataOne.school_id != g.access['user']['school_id']:
                    return {'error': 'Доступ запрещён.'}, 403
        elif g.access['user']['type'] in 'area':
            for dataOne in data:
                if dataOne.area_id != g.access['user']['area_id']:
                    return {'error': 'Доступ запрещён.'}, 403
        elif g.access['user']['type'] in 'region':
            areas = Area.query.filter_by(region_id=g.access['user']['region_id']).all()
            for dataOne in data:
                for area in areas:
                    if dataOne.area_id != area.id:
                        return {'error': 'Доступ запрещён.'}, 403
        elif g.access['user']['type'] in 'publisher':
            for dataOne in data:
                dataOne = getDataByPath({'requestedbooks': RequestedbookSchema().dump(dataOne.requestedbooks).data }, 'requestedbooks.book.publisher_id')
                if dataOne != g.access['user']['publisher_id']:
                    return {'error': 'Доступ запрещён.'}, 403        
        """-----------------------------------------------"""

        result = RequestSchema().dump(data, many=True).data
        if select:
            result = selectFields(result,select)
        return result

    @checkPermission(['librarian'])
    def post(self):
        """Create new Request"""
        try:
        # Нужна Проверка на то, являюсь ли я библиотекарем
            req = request.get_json(force=True)
            books = req['books']
            studyyearss = StudyYear.query.filter_by(current=True).first()
            if studyyearss:
                oldrequests = Request.query.filter_by(studyyear_id = studyyearss.id,school_id=g.access['user']['school_id']).first()            
            if oldrequests:
                Log(g.access,'Неудачная попытка созданий заявок', 5)
                return {"error": "для текущего года заявка уже есть"},405

            newRequest = Request(
                status=2,
                accepted = True,
                rejected_by_director = None,
                rejected_by_area = None,
                accepted_by_director = None,
                accepted_by_area = None,
                createdAt=time.time() * 1000,
                studyyear_id = studyyearss.id, 
                school_id=g.access['user']['school_id'] or req['school_id'],
                area_id=g.access['user']['school']['area_id'] or req['area_id']
            )
            db.session.add(newRequest)
            db.session.commit()
            for book in books:
                book['request_id'] = newRequest.id
                book['createdAt'] = time.time() * 1000
                studentCount = Student.query.filter_by(grad_id=book['grade_id']).count()
                grades = Grade.query.filter_by(id=book['grade_id']).first()
                book['students_count'] = studentCount
                book['school_id'] = grades.school_id
            data = [Requestedbook(**book) for book in books]
            for item in data:
                db.session.add(item)
            db.session.commit()
            Log(g.access,'Создано заявки', 1)
            return {"request": RequestSchema().dump(newRequest).data, 'books': [RequestedbookSchema().dump(item).data for item in data]}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий заявки', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['librarian'])
    def put(self):
        """Edit re-send by librarian""" 
        try:               
            req = request.get_json(force=True)
            data = Request.query.filter_by(id=req['id'], school_id=g.access['user']['school_id']).first()
            data.status = 2
            data.createdAt = time.time() * 1000
            data.accepted = False
            data.rejected_by_director = None
            data.rejected_by_area = None
            data.accepted_by_director = None
            data.accepted_by_area = None
            data.reason = ""
            db.session.commit()
            Log(g.access,'Редактировано заявки', 2)
            return {"request": RequestSchema().dump(data).data}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований заявки', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    # def delete(self):
    #     req = request.get_json(force=True)
    #     id = req['id']
    #     data = Request.query.filter_by(id=id).delete()
    #     # db.session.delete(data)
    #     db.session.commit()
    #     return "deleted"
    
class FinishRequest(Resource):

    @checkPermission(['librarian'])
    def post(self, request_id):
        try:
            reques = Request.query.filter_by(id=request_id, school_id=g.access['user']['school_id']).first()
            for requestedbook in reques.requestedbooks:
                if not requestedbook.accepted_count:
                    Log(g.access,'Неудачная попытка завершений заявки', 5)
                    return {"msg": "Запрошенные книги не выданы"}
            # reques.order_id = reques.requestedbooks[0].order_id
            reques.status = 6
            db.session.commit()
            Log(g.access,'Заявка завершена библеотекарем', 2)
            requests = Request.query.filter_by(order_id=reques.order_id).all()
            isFinish = True
            for req in requests:
                if req.status != 6:
                    isFinish = False
                    break
            if isFinish:
                order = Order.query.filter_by(id=reques.order_id).first()
                order.status = 3
                db.session.commit()
                Log(g.access,'Заказ книг завершен', 2)
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при завершений заявки', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class ResendByLibrarian(Resource):

    @checkPermission(['librarian'])
    def post(self):
        try:
            req = request.get_json(force=True)
            books = req['books']
            request_id = req['id']
            if not request_id and books:
                Log(g.access,'Неудачная попытка при пере отправке заявки библеотекарем', 5)
                raise "Error: Параметры не переданы"
            requests = Request.query.filter_by(id=request_id, school_id=g.access['user']['school_id']).first()
            requests.status = 2
            requestedbooks = Requestedbook.query.filter_by(request_id=request_id).delete()
            db.session.commit()
            for book in books:
                book['request_id'] = request_id
                book['createdAt'] = time.time() * 1000
                studentCount = Student.query.filter_by(grad_id=book['grade_id']).count()
                grades = Grade.query.filter_by(id=book['grade_id']).first()
                book['students_count'] = studentCount
                book['school_id'] = grades.school_id
            data = [Requestedbook(**book) for book in books]
            for item in data:
                db.session.add(item)
            db.session.commit()
            Log(g.access,'Заявка пере отправлено библеотекарем', 2)
            return {"request": RequestSchema().dump(requests).data, 'books': [RequestedbookSchema().dump(item).data for item in data]}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при пере отправке заявки библеотекарем', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class DirectorRequest(Resource):
    """Accept Request"""
    @checkPermission(['director'])
    def post(self, requestId):
        try:
            data = Request.query.filter_by(id=requestId, school_id=g.access['user']['school_id']).first()
            if(data.status != 2):
                Log(g.access,'Неудачная попытка при согласований заявки директором', 5)
                return {"msg": "Библеотекарь не подтвердил заявку"},409        
            data.status = 3
            data.accepted = True
            data.accepted_by_director = time.time() * 1000   
            data.rejected_by_director = None   
            db.session.commit()
            Log(g.access,'Согласовано директором', 2)
            return {"request": RequestSchema().dump(data).data} 
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при согласований заявки директором', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['director'])
    def put(self, requestId):
        try:
            args = request.args
            data = Request.query.filter_by(id=requestId, school_id=g.access['user']['school_id']).first()
            if(data.status != 2):
                Log(g.access,'Неудачная попытка при отклонений заявки директором', 5)
                return {"msg": "Библеотекарь не подтвердил заявку"}, 409        
            data.status = 1
            data.accepted = False
            data.reason = args.get('reason')
            data.rejected_by_director = time.time() * 1000
            data.accepted_by_director = None
            db.session.commit()
            Log(g.access,'Отклонено директором', 2)
            return {"request": RequestSchema().dump(data).data}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при отклонений заявки директором', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class AreaRequest(Resource):
    """Accept Request"""
    @checkPermission(['area'])
    def post(self, requestId):
        try:
            data = Request.query.filter_by(id=requestId, area_id=g.access['user']['area_id']).first()
            if(data.status != 3):
                Log(g.access,'Неудачная попытка при согласований заявки районом', 5)
                return {"msg": "Директор должен согласовать заявку"},409
            data.status = 4
            data.accepted = True
            data.accepted_by_area = time.time() * 1000
            data.rejected_by_area = None
            db.session.commit()
            Log(g.access,'Согласовано районом', 2)
            return {"request": RequestSchema().dump(data).data}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при согласований заявки районом', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['area'])
    def put(self, requestId):
        try:
            args = request.args
            data = Request.query.filter_by(id=requestId, area_id=g.access['user']['area_id']).first()
            if(data.status != 3):
                Log(g.access,'Неудачная попытка при отклонений заявки районом', 4)
                return {"msg": "Директор должен согласовать заявку"},409
            data.status = 1
            data.accepted = False
            data.reason = args.get('reason')
            data.rejected_by_area = time.time() * 1000
            data.accepted_by_area = None
            db.session.commit()
            Log(g.access,'Отклонено районом', 2)
            return {"request": RequestSchema().dump(data).data}
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при отклонений заявки районом', 4)
            return { 'msg': 'Запрос не обработан' }, 500
        

class AgregatedRequest(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        area_id = args.get('area_id')
        school_id = args.get('school_id')
        resultByarea = {}
        SortBy = args.get('sortBy')
        if SortBy:
            print(SortBy)
            args.pop('sortBy')
        if area_id:
            data = Request.query.filter_by(area_id=area_id).all()
        elif school_id:
            data = Request.query.filter_by(school_id=school_id).all()
        else:
            data = Request.query.all()

        """Проверка на доступ"""
        # if g.access['user']['type'] in ['director', 'librarian']:
        #     if school_id != g.access['user']['school_id']:
        #         return {'error': 'Доступ запрещён.'}, 403
        # elif g.access['user']['type'] in 'area':
        #     if area_id != g.access['user']['area_id']:
        #         return {'error': 'Доступ запрещён.'}, 403
        """-----------------------------------------------"""

        if SortBy == 'schools' and area_id != None:
            requestByschool = {}
            requests = [selectFields(RequestSchema().dump(item).data,'requestedbooks rejected_by_director accepted_by_area rejected_by_area accepted_by_director createdAt id status school') for item in data]
            order_id = getDataByPath({ "requests": requests },'requests.requestedbooks.order_id')
            for reques in requests:
                index = str(reques['id'])
                requestByschool[index] = requestByschool.get(index) or {'requested_count': 0, 'accepted_count': 0}
                reques['requestedbooks'] = selectFields(reques['requestedbooks'], 'requested_count accepted_count')
                reques['school'] = selectFields(reques['school'], 'id name_rus name_kaz')
                requestByschool[index]['rejected_by_director'] = reques['rejected_by_director']
                requestByschool[index]['accepted_by_director'] = reques['accepted_by_director']
                requestByschool[index]['accepted_by_area'] = reques['accepted_by_area']
                requestByschool[index]['rejected_by_area'] = reques['rejected_by_area']
                requestByschool[index]['createdAt'] = reques['createdAt']
                requestByschool[index]['status'] = reques['status']
                requestByschool[index]['id'] = reques['id']
                for requestedbook in reques['requestedbooks']:
                    requestByschool[index]['requested_count'] += requestedbook['requested_count'] or 0
                    requestByschool[index]['accepted_count'] += requestedbook['accepted_count'] or 0
                    requestByschool[index]['school'] = reques['school']
                if order_id:
                    order = Order.query.filter_by(id = order_id).first()
                    order = OrderSchema().dump(order).data                    
                    requestByschool[index]['data_receipt'] = order['createdAt']
                    requestByschool[index]['sendToPublisherTime'] = order['createdAt']
                else:
                    requestByschool[index]['data_receipt'] = None
                    requestByschool[index]['sendToPublisherTime'] = None
            resultByarea = { "data": [item for item in requestByschool.values()] }
        elif school_id and SortBy == 'schools':
            schoolRequests = {}
            requests = [selectFields(RequestSchema().dump(item).data,'requestedbooks rejected_by_director accepted_by_area rejected_by_area accepted_by_director createdAt id status school area_id') for item in data]
            order_id = getDataByPath({ "requests": requests },'requests.requestedbooks.order_id')
            for reques in requests:
                index = str(reques['id'])
                schoolRequests[index] = schoolRequests.get(index) or {'requested_count': 0, 'accepted_count': 0}
                reques['requestedbooks'] = selectFields(reques['requestedbooks'], 'requested_count accepted_count')
                reques['school'] = selectFields(reques['school'], 'id name_rus name_kaz')
                schoolRequests[index]['rejected_by_director'] = reques['rejected_by_director']
                schoolRequests[index]['accepted_by_director'] = reques['accepted_by_director']
                schoolRequests[index]['accepted_by_area'] = reques['accepted_by_area']
                schoolRequests[index]['rejected_by_area'] = reques['rejected_by_area']
                schoolRequests[index]['createdAt'] = reques['createdAt']
                schoolRequests[index]['status'] = reques['status']
                schoolRequests[index]['id'] = reques['id']
                schoolRequests[index]['school'] = reques['school']
                schoolRequests[index]['area_id'] = reques['area_id']
                for requestedbook in reques['requestedbooks']:
                    schoolRequests[index]['requested_count'] += requestedbook['requested_count'] or 0
                    schoolRequests[index]['accepted_count'] += requestedbook['accepted_count'] or 0
                
                if order_id:
                    order = Order.query.filter_by(id = order_id).first()
                    order = OrderSchema().dump(order).data                    
                    schoolRequests[index]['data_receipt'] = order['createdAt']
                    schoolRequests[index]['sendToPublisherTime'] = order['createdAt']
                else:
                    schoolRequests[index]['data_receipt'] = None
                    schoolRequests[index]['sendToPublisherTime'] = None
            resultByarea = { "data": [item for item in schoolRequests.values()] }         
        else:
            requests = [selectFields(RequestSchema().dump(item).data, 'id area_id area.name_rus area.name_kaz requestedbooks status') for item in data]            
            order_id = getDataByPath({ "requests": requests },'requests.requestedbooks.order_id')      
            for requestt in requests:
                requestt['requestedbooks'] = selectFields(requestt['requestedbooks'], 'requested_count shipped_count order_id')
                index = str(requestt['area_id'])
                resultByarea[index] = resultByarea.get(index) or { 'requested_count': 0, 'shipped_count': 0 }
                for requestedbook in requestt['requestedbooks']:
                    resultByarea[index]['requested_count'] += requestedbook['requested_count'] or 0
                    resultByarea[index]['shipped_count'] += requestedbook['shipped_count'] or 0

                resultByarea[index]['area'] = requestt['area']
                resultByarea[index]['id'] = requestt['id']
                resultByarea[index]['area_id'] = requestt['area_id']                                
                if order_id:          
                    order = Order.query.filter_by(id=order_id).first()
                    orders = OrderSchema().dump(order).data
                    resultByarea[index]['createdAt'] = orders['createdAt']
                    if requestt['status'] == 6:
                        resultByarea[index]['status'] = 1
                else:
                    resultByarea[index]['createdAt'] = None
                    resultByarea[index]['status'] = 0
            resultByarea = { "data": [item for item in resultByarea.values()] }

        return resultByarea

# class DeclineRequest(Resource):
#     """Decline Request"""
#     @checkPermission()
#     def post(self, requestId):
#         request = Request.query.filter_by(id=requestId).first()
#         userType = geta(g.access, 'user.type')
#         access = {
#             'librarian': 1,
#             'director': 2,
#             'school': 2,
#             'area': 3
#         }
#         if userType and (access[userType] == request.status):
#             request.status -= 1
#             request.confirmedAt = time.time() * 1000
#             db.session.commit()
#         return RequestSchema().dump(request).data
