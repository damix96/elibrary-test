import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, editFields, selectFields, RequestedbookSchema, Requestedbook, Area, School, getDataByPath, BookSchema, Order, OrderSchema
from resources.Auth import checkPermission
from resources.Logs import Log


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()

class Requestedbooks(Resource):
    """CRUD"""
    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select']
        data = Requestedbook.query.filter_by(**args).all()
        """Проверка на доступ"""
        if g.access['user']['type'] in ['director', 'librarian']:
            for dataOne in data:
                if dataOne.school_id != g.access['user']['school_id']:
                    return {'error': 'Доступ запрещён.'}, 403
        elif g.access['user']['type'] in 'region':
            areas = Area.query.filter_by(region_id=g.access['user']['region_id']).all()
            for area in areas:
                schools = School.query.filter_by(area_id=area.id).all()
                for dataOne in data:
                    for school in schools:
                        if dataOne.school_id != school.id:
                            return {'error': 'Доступ запрещён.'}, 403
        elif g.access['user']['type'] in 'publisher':
            for dataOne in data:
                dataOne = getDataByPath({'book': BookSchema().dump(dataOne.book).data }, 'book.publisher_id')
                if dataOne != g.access['user']['publisher_id']:
                    return {'error': 'Доступ запрещён.'}, 403                
        """-----------------------------------------------"""

        result = [RequestedbookSchema().dump(item).data for item in data] 
        if select:
            result = selectFields(result, select)
        return result
    
    @checkPermission(['librarian'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            data = Requestedbook(**req)
            db.session.add(data)
            db.session.commit()
            # Log(g.access,'Создан запрос книг', 1)
            return RequestedbookSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            # Log(g.access,'Ошибка при созданий запрос книг', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    @checkPermission(['librarian'])
    def put(self):
        # try:
        req = request.get_json(force=True)
        id = req['id']
        # Проверка мой ли учебник
        data = Requestedbook.query.filter_by(id=id, school_id=g.access['user']['school_id']).first()
        if req['accepted_count']:
            data.shipped_count = req['accepted_count']
        editFields(data, req, ignore='grade book request request_id')
        db.session.commit()
        Log(g.access,'Редактировано запрос книг', 2)
        return RequestedbookSchema().dump(data).data, 200
        # except:
        #     db.session.rollback()
        #     db.session.flush()
        #     Log(g.access,'Ошибка при редактирований запрос книг', 4)
        #     return { 'msg': 'Запрос не обработан' }, 500


class DeleteRequestedBook(Resource):
    @checkPermission(['librarian'])
    def delete(self, id):        
        try:
            data = Requestedbook.query.filter_by(id=id, school_id=g.access['user']['school_id']).delete()
            db.session.commit()
            # Log(g.access,'Удалено область', 3)
            return "deleted"
        except:
            db.session.rollback()
            db.session.flush()
            # Log(g.access,'Ошибка при удалений области', 4)
            return { 'msg': 'Запрос не обработан' }, 500


class sortedRequestedBooks(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self, orderId):  
        args = {**request.args}
        sortBy = args.get('sortBy') or 'books' # books/schools
        args.pop('sortBy')
        data = Requestedbook.query.filter_by(order_id=orderId, **args).all()        
        
        """Проверка на доступ"""
        # if g.access['user']['type'] in ['director', 'librarian']:
        #     for dataOne in data:
        #         if dataOne.school_id != g.access['user']['school_id']:
        #             return {'error': 'Доступ запрещён.'}, 403
        # elif g.access['user']['type'] in 'area':
        #     schools = School.query.filter_by(area_id=g.access['user']['area_id']).all()
        #     for dataOne in data:
        #         for school in schools:
        #             if dataOne.school_id != school.id:
        #                 return {'error': 'Доступ запрещён.'}, 403
        # elif g.access['user']['type'] in 'publisher':
        #     for dataOne in data:
        #         dataOne = getDataByPath({'book': BookSchema().dump(dataOne.book).data }, 'book.publisher_id')
        #         if dataOne != g.access['user']['publisher_id']:
        #             return {'error': 'Доступ запрещён.'}, 403     
        """-----------------------------------------------"""

        data = [RequestedbookSchema().dump(item).data for item in data]
        if sortBy == "books":
            book = {} 
            for item in data:
                index = str(item['book']['id'])
                book[index] = book.get(index) or { 'requested_count': 0, 'accepted_count': 0 , 'request_id': 0}
                book[index]['requested_count'] += item['requested_count'] or 0
                book[index]['accepted_count'] += item['accepted_count'] or 0
                book[index]['book'] = selectFields(item['book'], '-book.grade -book.subject_id -subject -publisher_id -publisher ')
                book[index]['request_id'] = item['request_id']
            # print(book)
        elif sortBy == "schools":
            book = {} 
            for item in data:
                index = str(item['request']['school_id'])
                book[index] = book.get(index) or { 'requested_count': 0, 'accepted_count': 0 , 'request_id': 0}
                book[index]['requested_count'] += item['requested_count'] or 0
                book[index]['accepted_count'] += item['accepted_count'] or 0
                book[index]['school'] = item['request']['school']
                book[index]['request_id'] = item['request_id']
            # print(book)
        return { "data": [item for item in book.values()] }            