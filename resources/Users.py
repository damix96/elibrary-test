import uuid
import time
import pprint
from flask import request, jsonify, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Access, User, UserSchema, AccessSchema, editFields, selectFields
from resources.Auth import checkPermission
from resources.Logs import Log

pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()
userSchema = UserSchema()
accessSchema = AccessSchema()

def edit():
    try:
        req = request.get_json(force=True)
        if not req:
            Log(g.access,'Неудачная попытка редактирований пользователя', 4)            
            return {'error': 'Данные должны быть в формате JSON'}, 400
        user = User.query.filter_by(id=req['id']).first()
        if not user:
            Log(g.access,'Неудачная попытка редактирований пользователя', 4)            
            return {'error': 'Такой пользователь не найден'}, 404
        pp(accessSchema.dump(g.access).data)
        if not (int(g.access['user']['id']) == int(req['id']) or g.access['user']['type'] in ['admin', 'moderator']):
            Log(g.access,'Неудачная попытка редактирований пользователя', 4)            
            return {'error': 'Доступ запрещён.'}, 403    
        if 'login' in req and g.access['user']['type'] in ['admin','moderator']:
            users = User.query.filter_by(login=req['login']).first()
            if users != None and users.login != user.login:
                Log(g.access,'Неудачная попытка редактирований пользователя', 4)
                return {'error': 'Введенный логин занят'}, 405            
        editFields(user, req, ignore='password type')                
        edited = userSchema.dump(user).data
        db.session.commit()
        Log(g.access,'Редактирован пользователь', 2)
        return { 'data': edited }, 200
    except:
        db.session.rollback()
        db.session.flush()
        Log(g.access,'Ошибка при редактирований пользователя', 4)
        return { 'msg': 'Запрос не обработан' }, 500

def editPasswordAdmin():
    try:
        req = request.get_json(force=True)
        if not req:
            Log(g.access,'Неудачная попытка изменений пароля', 4)            
            return {'error': 'Данные должны быть в формате JSON'}, 400
        user = User.query.filter_by(id=req['id']).first()
        if not user:
            Log(g.access,'Неудачная попытка изменений пароля', 4)            
            return {'error': 'Такой пользователь не найден'}, 404
        if not (int(g.access['user']['id']) == int(req['id']) or g.access['user']['type'] in ['admin', 'moderator']):
            Log(g.access,'Неудачная попытка изменений пароля', 4)            
            return {'error': 'Доступ запрещён.'}, 403
        if 'newPassword' in req:
            if g.access['user']['type'] == 'moderator' and g.access['user']['id'] != user.id and user.type in ['admin', 'moderator']:
                        Log(g.access,'Неудачная попытка изменений пароля', 4)                    
                        return { 'error': 'Доступ запрещён' }, 403
            if not User.check_password(user, req.get('oldPassword')):                
                if not g.access['user']['type'] in ['admin', 'moderator']:
                    Log(g.access,'Неудачная попытка изменений пароля', 4)                    
                    return { 'error': 'Неверный старый пароль' }, 405
                if g.access['user']['type'] in ['admin', 'moderator'] and g.access['user']['id'] == int(req['id']):
                    Log(g.access,'Неудачная попытка изменений пароля', 4)                    
                    return { 'error': 'Неверный старый пароль' }, 405                
            if (User.check_password(user, req.get('newPassword'))):
                if not g.access['user']['type'] in ['admin', 'moderator']:
                    Log(g.access,'Неудачная попытка изменений пароля', 4)
                    return { 'error': 'Новый пароль идентичен старому паролю' }, 405
                if g.access['user']['type'] in ['admin', 'moderator'] and g.access['user']['id'] == int(req['id']):
                    Log(g.access,'Неудачная попытка изменений пароля', 4)
                    return { 'error': 'Новый пароль идентичен старому паролю' }, 405                
            if len(req['newPassword']) < 4:
                Log(g.access,'Неудачная попытка изменений пароля', 4)
                return { 'error': 'Пароль слишком короткий' }, 400
            user.password = User.hash_password(
                req['newPassword']).decode('utf-8')
        edited = userSchema.dump(user).data
        db.session.commit()
        Log(g.access,'Изменен пароль', 2)
        return { 'data': edited }, 200
    except:
        db.session.rollback()
        db.session.flush()
        Log(g.access,'Ошибка при изменений пароля', 4)
        return { 'msg': 'Запрос не обработан' }, 500

    # pp(request.params)
def getByParams():
    args = { **request.args }
    select = args.get('select')
    if select:
        del args['select']
    users = User.query.filter_by(**args).all()
    result = [userSchema.dump(item).data for item in users]
    if not users:
        return {}, 404
    if select:
        result = selectFields(result,select)
    return result


def create():
    try:
        data = request.get_json(force=True)
        fields = selectFields(data, '-password')
        fields['password'] = User.hash_password(data['password']).decode('utf-8'),
        user = User(**fields)
        db.session.add(user)
        db.session.commit()
        result = userSchema.dump(user).data, 200
        Log(g.access,'Пользователь создан', 1)
        return result
    except:
        db.session.rollback()
        db.session.flush()
        Log(g.access,'Ошибка при созданий пользователя', 4)
        return { 'msg': 'Запрос не обработан' }, 500

    # print(User.hash_password('damir'))

def checkLogin():
    data = request.get_json(force=True)
    users = User.query.filter_by(login=data['login']).first()    
    if users != None:
        return { 'error': 'Логин используется' }, 401
    else: 
        return "success"


class Users(Resource):
    @checkPermission()
    def get(self):
        """Get specific user info"""
        return getByParams()

    # @checkPermission([])
    def post(self):
         return create()

    @checkPermission()
    def put(self):
        """Edit User"""
        return edit()
    
    @checkPermission(['admin'])
    def delete(self):
        try:
            req = request.get_json(force=True)
            id = req['id']
            data = User.query.filter_by(id=id).first()
            db.session.delete(data)
            db.session.commit()
            Log(g.access,'Пользователь удален', 3)
            return "deleted"
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при удалений пользователя', 4)
            return { 'msg': 'Запрос не обработан' }, 500

class checkUser(Resource):
    def post(self):
        return checkLogin()

class changePassword(Resource):
    @checkPermission()
    def put(self):
        return editPasswordAdmin()        
