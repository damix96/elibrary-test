""" Модуль Районов """

import time
import pprint
from flask import request, g
from flask_restful import Resource
from flask_ipinfo import IPInfo
from Model import db, Subject, editFields, SubjectSchema, selectFields
from resources.Auth import checkPermission
from resources.Logs import Log


pp = pprint.PrettyPrinter(width=41, compact=True).pprint
ipInfo = IPInfo()


class Subjects(Resource):

    @checkPermission(['admin', 'director', 'area', 'region', 'publisher', 'librarian','moderator'])
    def get(self):
        # return 1
        """Get specific info"""
        args = {**request.args}
        select = args.get('select')
        if select:
            del args['select'] 
        data = Subject.query.filter_by(**args).all()
        result = [SubjectSchema().dump(item).data for item in data]
        print(result)
        if select: 
            result = selectFields(result, select)
        return result

    @checkPermission(['admin','moderator'])
    def post(self):
        """ create """
        try:
            req = request.get_json(force=True)
            subject = Subject(**req)
            db.session.add(subject)
            db.session.commit()
            Log(g.access,'Создан предмет', 1)
            return { 'data': 'Успех!' }, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при созданий предмета', 4)
            return { 'msg': 'Запрос не обработан' }, 500

    @checkPermission(['admin','moderator'])
    def put(self):
        try:
            req = request.get_json(force=True)
            id = req['id']
            # Проверка мой ли учебник
            data = Subject.query.filter_by(id=id).first()
            editFields(data, req)
            db.session.commit()
            Log(g.access,'Редактирован предмет', 2)
            return SubjectSchema().dump(data).data, 200
        except:
            db.session.rollback()
            db.session.flush()
            Log(g.access,'Ошибка при редактирований предмета', 4)
            return { 'msg': 'Запрос не обработан' }, 500
    
    # @checkPermission(['admin'])
    # def delete(self):
    #     req = request.get_json(force=True)
    #     id = req['id']
    #     data = Subject.query.filter_by(id=id).first()
    #     db.session.delete(data)
    #     db.session.commit()
    #     return "deleted"