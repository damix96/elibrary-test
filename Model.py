import pprint
import inspect
from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
# from flask_sqlalchemy.dialects import postgresql
from flask_bcrypt import generate_password_hash, check_password_hash
from Select import selectFields, getDataByPath

db = SQLAlchemy()
ma = Marshmallow()
 

# def selectFields(data, exclude='', select=''):
#     result = {}
#     exclude = exclude.split()
#     select = select.split()
#     for key in data:
#         if not (key in exclude):
#             result[key] = data[key]
#     return result



def editFields(target, data, ignore=''):
    exclude = ignore.split()
    for key in data:
        if not (key in exclude):
            setattr(target, key, data[key])

class Access(db.Model):
    """Токены (Доступы) """
    __tablename__ = 'accesses'
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.Text, unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('user', uselist=False))
    ip = db.Column(db.Text, nullable=True)
    os = db.Column(db.Text, nullable=True)
    device = db.Column(db.Text, nullable=True)
    browser = db.Column(db.Text, nullable=True)
    createdAt = db.Column(db.Float, nullable=True)

class Logging(db.Model):
    """Логирование"""
    __tablename__ = 'logs'
    id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.Text, nullable=False)
    userLogin = db.Column(db.Text, nullable=True)
    userType = db.Column(db.Text, nullable=True)
    ip = db.Column(db.Text, nullable=True)
    os = db.Column(db.Text, nullable=True)
    browser = db.Column(db.Text, nullable=True)
    createdAt = db.Column(db.Float, nullable=True)
    typeLog = db.Column(db.Integer, nullable=False)

class User(db.Model):
    """Юзеры"""
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.Text, unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    type = db.Column(db.Text, nullable=False)
    email = db.Column(db.Text, nullable=True)
    phone = db.Column(db.Text, nullable=True)
    firstname = db.Column(db.Text, nullable=True)
    lastname = db.Column(db.Text, nullable=True)
    fathername = db.Column(db.Text, nullable=True)
    region_id = db.Column(db.Integer, db.ForeignKey('regions.id'), nullable=True)
    # region = db.relationship('Region', back_populates='id')
    # region = db.relationship('Region', backref=db.backref('user.region', uselist=False))
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'), nullable=True)
    # area = db.relationship('Area', backref=db.backref('area', uselist=False))
    publisher_id = db.Column(db.Integer, db.ForeignKey('publishers.id'), nullable=True)
    # publisher = db.relationship('Publisher', backref=db.backref('publisher', uselist=False))
    school_id = db.Column(db.Integer, db.ForeignKey('schools.id'), nullable=True)
    school = db.relationship('School', backref=db.backref('school', uselist=False))

    def check_password(self, password):
        return False if (not password) else check_password_hash(self.password, password)

    @staticmethod
    def hash_password(password):
        return generate_password_hash(password)

class Region(db.Model):
    """Области"""
    __tablename__ = 'regions'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    address_rus = db.Column(db.Text, nullable=True)
    address_kaz = db.Column(db.Text, nullable=True)
    bin = db.Column(db.Text, nullable=True)
    email = db.Column(db.Text, nullable=True)
    phone = db.Column(db.Text, nullable=True)

class Area(db.Model):
    """Районы"""
    __tablename__ = 'areas'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    address_rus = db.Column(db.Text, nullable=True)
    address_kaz = db.Column(db.Text, nullable=True)
    bin = db.Column(db.Text, nullable=True)
    email = db.Column(db.Text, nullable=True)
    phone = db.Column(db.Text, nullable=True)
    region_id = db.Column(db.Integer, db.ForeignKey('regions.id'), nullable=False)
    region = db.relationship('Region', backref=db.backref(name='area.region', uselist=False))
    # user = db.relationship('User', backref=db.backref(name='area.user', uselist=False))

class Publisher(db.Model):
    """Издательства"""
    __tablename__ = 'publishers'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    address_rus = db.Column(db.Text, nullable=True)
    address_kaz = db.Column(db.Text, nullable=True)
    bin = db.Column(db.Text, nullable=True)
    email = db.Column(db.Text, nullable=True)
    phone = db.Column(db.Text, nullable=True)
    # area_id = db.Column(db.Integer, db.ForeignKey('areas.id'), nullable=False)
    # area = db.relationship('Area', backref=db.backref('publisher.area', uselist=False))    

class Kato(db.Model):
    """Като"""
    __tablename__ ='katos'
    id = db.Column(db.Integer,primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'), nullable=True)

class School(db.Model):
    """Школы"""
    __tablename__ = 'schools'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    address_rus = db.Column(db.Text, nullable=True)
    address_kaz = db.Column(db.Text, nullable=True)
    bin = db.Column(db.Text, nullable=True)
    email = db.Column(db.Text, nullable=True)
    phone = db.Column(db.Text, nullable=True)
    grade_from = db.Column(db.Integer, nullable=True)
    grade_to = db.Column(db.Integer, nullable=True)
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'), nullable=False)
    area = db.relationship('Area', backref=db.backref('school.area', uselist=False))
    languages = db.Column(db.ARRAY(db.Text), nullable=True)
    kato_id = db.Column(db.Integer, db.ForeignKey('katos.id'), nullable=True)
    kato = db.relationship('Kato', backref=db.backref('school.kato', uselist=False))

class Grade(db.Model):
    """Классы"""
    __tablename__ = 'grades'
    id = db.Column(db.Integer, primary_key=True)
    grade = db.Column(db.Integer, nullable=True)
    grade_letter = db.Column(db.String(15), nullable=True)
    curator = db.Column(db.Text, nullable=True)
    school_id = db.Column(db.Integer, db.ForeignKey('schools.id'), nullable=False)
    school = db.relationship('School', backref=db.backref('grade.school', uselist=False))
    students_count = db.Column(db.Integer, nullable=True)
    language_id = db.Column(db.Integer, db.ForeignKey(
        'languages.id'), nullable=False)
    language = db.relationship(
        'Language', backref=db.backref('grade.language', uselist=False))
    studyyear_id = db.Column(db.Integer, db.ForeignKey(
        'studyyear.id'), nullable=False)


class Student(db.Model):
    "Студент"
    __tablename__ = 'students'
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.Text, nullable=True)
    lastname = db.Column(db.Text, nullable=True)
    fathername = db.Column(db.Text, nullable=True)
    iin = db.Column(db.Text,nullable=True)
    grad_id = db.Column(db.Integer, db.ForeignKey('grades.id'), nullable=False)
    grade = db.relationship('Grade', backref=db.backref('student.grade', uselist=False))
    school_id = db.Column(db.Integer, db.ForeignKey('schools.id'), nullable=True)

class StudyYear(db.Model):
    "Года"
    __tablename__ = 'studyyear'
    id = db.Column(db.Integer, primary_key=True)
    period = db.Column(db.Text, nullable=True, unique = True)    
    current = db.Column(db.Boolean, nullable=True)

class Subject(db.Model):
    """Предметы"""
    __tablename__ = 'subjects'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)

class Language(db.Model):
    """Язык"""
    __tablename__ = 'languages'
    id = db.Column(db.Integer, primary_key=True)
    name_rus = db.Column(db.Text, nullable=True)
    name_kaz = db.Column(db.Text, nullable=True)
    # type = db.Column(db.Text, nullable=True)
    
class Book(db.Model):
    """Книги"""
    __tablename__ = 'books'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=True)
    authors = db.Column(db.Text, nullable=True)
    year = db.Column(db.Integer, nullable=True)
    grade = db.Column(db.Integer, nullable=True)
    subject_id = db.Column(db.Integer, db.ForeignKey('subjects.id'), nullable=False)
    subject = db.relationship('Subject', backref=db.backref('book.subject', uselist=False))
    publisher_id = db.Column(db.Integer, db.ForeignKey('publishers.id'), nullable=False)
    publisher = db.relationship('Publisher', backref=db.backref('book.publisher', uselist=False))
    language_id = db.Column(db.Integer, db.ForeignKey('languages.id'), nullable=False)
    language = db.relationship('Language', backref=db.backref('book.language', uselist=False))
    active = db.Column(db.Boolean)

class Students_Book(db.Model):
    """Выданная ученику книга"""
    __tablename__ = 'students_book'
    id = db.Column(db.Integer, primary_key=True)
    book_id =  db.Column(db.Integer, db.ForeignKey('books.id'), nullable=False)
    book = db.relationship('Book', backref=db.backref('students_book.book', uselist=False))
    createdAt = db.Column(db.Float, nullable=True)
    status = db.Column(db.String(5), nullable=True)
    student_id = db.Column(db.Integer, db.ForeignKey('students.id'), nullable=False)
    grade_id = db.Column(db.Integer, db.ForeignKey('grades.id'), nullable=False)
    # request_id = db.Column(db.Integer, db.ForeignKey(
    #     'requests.id'), nullable=False)
    studyyear_id = db.Column(db.Integer, db.ForeignKey(
        'studyyear.id'), nullable=False)



class Formbook(db.Model):
    """Форма 400"""
    __tablename__ = 'formbooks'
    id = db.Column(db.Integer, primary_key=True)
    book_id = db.Column(db.Integer, db.ForeignKey(
        'books.id'), nullable=False)
    language_id = db.Column(db.Integer, db.ForeignKey(
        'languages.id'), nullable=False)
    grade = db.Column(db.Integer, nullable=True)
    studyyear_id = db.Column(db.Integer, db.ForeignKey('studyyear.id'), nullable=False)
    book = db.relationship(
        'Book', backref=db.backref('formbook.book', uselist=False))
    Language = db.relationship(
        'Language', backref=db.backref('formbook.language', uselist=False))

class Request(db.Model):
    """Заявка на книги"""
    __tablename__ = 'requests'
    id = db.Column(db.Integer, primary_key=True)
    reason = db.Column(db.Text, nullable=True)
    status = db.Column(db.Integer, nullable=False)
    accepted = db.Column(db.Boolean, nullable=True)
    rejected_by_director = db.Column(db.Float, nullable=True)
    rejected_by_area = db.Column(db.Float, nullable=True)
    createdAt = db.Column(db.Float, nullable=True)
    accepted_by_director = db.Column(db.Float, nullable=True)
    accepted_by_area = db.Column(db.Float, nullable=True)
    studyyear_id = db.Column(db.Integer, db.ForeignKey('studyyear.id'), nullable=False)

    # shipped_count = db.Column(db.Integer, nullable=True)
    
    school_id = db.Column(db.Integer, db.ForeignKey('schools.id'), nullable=False)
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'), nullable=True)
    area = db.relationship(
        'Area', backref = db.backref('request.area')
    )    
    # order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=True)
    school = db.relationship(
        'School', backref=db.backref('request.school', uselist=False))

    requestedbooks = db.relationship(
        'Requestedbook', backref=db.backref('request.requestedbooks', uselist=True))
    order_id = db.Column(db.Integer, db.ForeignKey(
        'orders.id'), nullable=True)
    

class Requestedbook(db.Model):
    """Запрошеные на книги"""
    __tablename__ = 'requestedbooks'
    id = db.Column(db.Integer, primary_key=True)
    students_count = db.Column(db.Integer, nullable=True)
    requested_count = db.Column(db.Integer, nullable=True)
    accepted_count = db.Column(db.Integer, nullable=True)
    shipped_count = db.Column(db.Integer, nullable=True)

    createdAt = db.Column(db.Float, nullable=True)
    gotAt = db.Column(db.Float, nullable=True)

    order_id = db.Column(db.Integer, db.ForeignKey(
        'orders.id'), nullable=True)            
    request_id = db.Column(db.Integer, db.ForeignKey(
        'requests.id'), nullable=False)
    request = db.relationship(
        'Request', backref=db.backref('requestedbook.request', uselist=False))
    grade_id = db.Column(db.Integer, db.ForeignKey(
        'grades.id'), nullable=True)
    grade = db.relationship(
        'Grade', backref=db.backref('requestedbook.grade', uselist=False))
    book_id = db.Column(db.Integer, db.ForeignKey(
        'books.id'), nullable=True)
    book = db.relationship(
        'Book', backref=db.backref('requestedbook.book', uselist=False))
    school_id = db.Column(db.Integer, db.ForeignKey(
        'schools.id'), nullable=False)


class Order(db.Model):
    """Запрос на покупку книг"""
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Integer, nullable=False)
    shippedAt = db.Column(db.Float, nullable=True)
    createdAt = db.Column(db.Float, nullable=True)
    requested_count = db.Column(db.Integer, nullable=False)
    shipped_count = db.Column(db.Integer, nullable=True)
    studyyear_id = db.Column(db.Integer, db.ForeignKey(
        'studyyear.id'), nullable=False)
    publisher_id = db.Column(db.Integer, db.ForeignKey(
        'publishers.id'), nullable=False)
    publisher = db.relationship(
        'Publisher', backref=db.backref('order.publisher', uselist=False))
    area_id = db.Column(db.Integer, db.ForeignKey(
        'areas.id'), nullable=True)
    area = db.relationship(
        'Area', backref=db.backref('order.area', uselist=False))
    # school_id = db.Column(db.Integer, db.ForeignKey('schools.id'), nullable=True)
    # request_ids = db.Column(db.ARRAY(db.Integer), nullable=True)
    # requests = db.relationship(
    #     'Request', backref=db.backref('order.requests', uselist=True))

    # school = db.relationship(
    #     'School', backref=db.backref('order.school', uselist=False))
    # requests = db.relationship(
    #     'Request', backref=db.backref('order.requests', uselist=True))

# class Suborder(db.Model):
#     """Запрос на покупку книг"""
#     __tablename__ = 'orders'
#     id = db.Column(db.Integer, primary_key=True)
#     publisher_id = db.Column(db.Integer, db.ForeignKey('publishers.id'), nullable=False)
#     order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=True)
#     request_id = db.Column(db.Integer, db.ForeignKey('requests.id'), nullable=True)
#     # publisher = db.relationship(
#     #     'Publisher', backref=db.backref('suborder.publisher', uselist=False))
#     # order = db.relationship(
#     #     'Order', backref=db.backref('suborder.order', uselist=False))


### MODEL SCHEMAS ### 

class RegionSchema(ma.Schema):
    class Meta:
        model = Region
        fields = [
            'id',
            'name_rus',
            'name_kaz',
            'address_rus',
            'address_kaz',
            'email',
            'phone',
            'bin'
        ]        

# class UserByAreaSchema(ma.Schema):
#     class Meta:
#         model = User
#         fields = [
#             'id',
#             'login',
#             # 'password',
#             'type',
#             'email',
#             'phone',
#             'firstname',
#             'lastname',
#             'fathername'
#         ]
#         exclude = ['password']

class AreaSchema(ma.Schema):
    class Meta:
        model = Area
        fields = [
            'id',
            'name_rus',
            'name_kaz',
            'address_rus',
            'address_kaz',
            'email',
            'phone',
            'region',
            'region_id',
            'bin'
            # 'user'
            # 'school_count'
        ]
    region = ma.Nested(RegionSchema)
    # user = ma.Nested(UserByAreaSchema)
    


class PublisherSchema(ma.Schema):
    class Meta:
        model = Publisher
        fields = [
            'id',
            'name_rus',
            'name_kaz',
            'address_rus',
            'address_kaz',
            'email',
            'phone',
            # 'user',
            # 'area_id',
            # 'area',
            'bin'
        ]
    region = ma.Nested(RegionSchema)
    
class KatoSchema(ma.Schema):
    class Meta:
        fields = [
            'id',
            'name_rus',
            'name_kaz',
            'area_id'
        ]
class LogSchema(ma.Schema):
    class Meta:
        model = Logging
        fields = [
            'id',
            'userLogin',
            'userType',
            'ip',
            'os',
            'browser',
            'createdAt',
            'action',
            'typeLog'
        ]        

class SchoolSchema(ma.Schema):
    class Meta:
        model = School
        fields = [
            'id',
            'name_rus',
            'name_kaz',
            'address_rus',
            'address_kaz',
            'email',
            'phone',
            'grade_from',
            'grade_to',
            'area_id',
            'area',
            'bin',
            'languages',
            'kato_id',
            'kato'
        ]
    region = ma.Nested(RegionSchema)
    area = ma.Nested(AreaSchema)
    kato = ma.Nested(KatoSchema)
    


class UserSchema(ma.Schema):
    class Meta:
        model = User
        fields = [
            'id',
            'login',
            # 'password',
            'type',
            'email',
            'phone',
            'firstname',
            'lastname',
            'fathername',
            'region_id',
            'area_id',
            # 'area',
            'school_id',
            'publisher_id',
            'school'
        ]
        exclude = ['password']
    school = ma.Nested(SchoolSchema)
    area = ma.Nested(AreaSchema)

    


class AccessSchema(ma.Schema):
    class Meta:
        model = Access
        fields = [
            'id',
            'token',
            'user',
            'user_id',
            'os',
            'ip',
            'device',
            'browser',
            'createdAt'
        ]
    user = ma.Nested(UserSchema)

    


class SubjectSchema(ma.Schema):
    class Meta:
        model = Subject
        fields = [
            'id',
            'name_rus',
            'name_kaz'
        ]


class LanguageSchema(ma.Schema):
    class Meta:
        model = Language
        fields = [
            'id',
            'name_rus',
            'name_kaz'
        ]


class GradeSchema(ma.Schema):
    class Meta:
        model = Grade
        fields = [
            'id',
            'grade',
            'grade_letter',
            'curator',
            'school_id',
            # 'school',
            'students_count',
            'language_id',
            # 'studyyear',
            'language',
            'students_count'
        ]
    language = ma.Nested(LanguageSchema)



class BookSchema(ma.Schema):
    class Meta:
        model = Book
        fields = [
            'id',
            'name',
            'authors',
            'year',
            'grade',
            'subject_id',
            'subject',
            'publisher_id',
            'publisher',
            'language_id',
            'language',
            'active'
        ]
    language = ma.Nested(LanguageSchema)
    publisher = ma.Nested(PublisherSchema)
    subject = ma.Nested(SubjectSchema)




class FormbookSchema(ma.Schema):
    class Meta:
        model = Formbook
        fields = [
            'id',
            'book_id',
            'book',
            'language_id',
            'language',
            'studyyear_id',
            'grade'
        ]
    book = ma.Nested(BookSchema)
    language = ma.Nested(LanguageSchema)


class OrderSchema(ma.Schema):
    class Meta:
        model = Order
        fields = [
            'id',
            'status',
            'shippedAt',
            'createdAt',
            'publisher_id',
            'requested_count',
            'shipped_count',
            'publisher',
            'area_id',
            'studyyear_id',
            'area'
        ]
    area = ma.Nested(AreaSchema)
    publisher = ma.Nested(PublisherSchema)

    

# class SuborderSchema(ma.Schema):
#     class Meta:
#         model = Suborder
#         fields = [
#             'id',
#             'order_id',
#             'request_id',
#             'publisher_id'
#         ]

class SortedSchoolSchema(ma.Schema):
    class Meta:
        model = School
        fields = [
            'id',
            'name_rus',
            'name_kaz',
        ]

class SortedBooksSchema(ma.Schema):
    class Meta:
        model = Book
        fields = [
            'id',
            'name',
            'authors',
            'year',        
        ]

class RequestForSortingSchema(ma.Schema):
    class Meta:
        model = Requestedbook
        fields = [
            'id',
            'school_id',
            'school',
        ]
    school = ma.Nested(SortedSchoolSchema)



class RequestedbookSchema(ma.Schema):
    class Meta:
        model = Requestedbook
        fields = [
            'id',
            'requested_count',
            'students_count',
            'accepted_count',
            'shipped_count',
            'createdAt',
            'order_id',
            'request_id',
            'request',
            'grade_id',
            'grade',
            'book',
            'book_id',
            'gotAt',
            'school_id'
        ]
    grade = ma.Nested(GradeSchema)
    book = ma.Nested(BookSchema)
    request = ma.Nested(RequestForSortingSchema)
    
    


class RequestSchema(ma.Schema):
    class Meta:
        model = Request
        fields = [
            'id',
            'status',
            'accepted',
            'rejected_by_director',
            'rejected_by_area',
            'accepted_by_director',
            'accepted_by_area',
            'createdAt',
            'reason',
            # 'confirmedAt',
            'school_id',
            'school',
            'area_id',
            'area',
            'studyyear_id',
            # 'order_id',
            'requestedbooks'
        ]
    school = ma.Nested(SchoolSchema)
    requestedbooks = ma.Nested(RequestedbookSchema, many=True)
    area = ma.Nested(AreaSchema)

class StudentSchema(ma.Schema):
    class Meta:
        model = Request
        fields = [
            'id',
            'firstname',
            'lastname',
            'fathername',
            'iin',
            'grad_id',
            'school_id'
            # 'grade'
        ]

class Students_BookSchema(ma.Schema):
    class Meta:
        fields = [
            'id',
            'book_id',
            'book',
            'createdAt',
            'status',
            'student_id',
            'studyyear_id',
            'grade_id'
            # 'request_id',
            # 'request'
        ]
    book = ma.Nested(BookSchema)
    # request = ma.Nested(RequestSchema)

class StudyYearSchema(ma.Schema):
    class Meta:
        fields = [
            'id',
            'period',
            'current'            
        ]
















# def createSchema(aModel, aExclude=[], aFields=None):
#     class Schema(ma.Schema):
#         class Meta:
#             model = aModel
#             exclude = aExclude
#             # fields = aFields or ma.ModelConverter(aModel)
    
#     return Schema()


# def createUserSchema(f=['id', 'login', 'email', 'phone', 'firstname', 'lastname', 'fathername']):
#     class Sch(ma.Schema):
#         class Meta:
#             model = User
#             exclude = ['password']
#             fields = f
#     return Sch()


# def createAccessSchema(f=['id', 'token', 'user', 'user_id', 'os', 'ip', 'device', 'browser', 'createdAt']):
#     class Sch(ma.Schema):
#         class Meta:
#             model = Access
#             fields = f 
#         user = ma.Nested(UserSchema)
#     return Sch()
